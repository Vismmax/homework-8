const { Router } = require('express');
const PostService = require('../services/postService');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.get(
  '/',
  (req, res, next) => {
    try {
      res.data = PostService.getPosts();
    } catch (err) {
      res.err = err;
      res.err.status = 404;
    } finally {
      next();
    }
  },
  responseMiddleware,
);

router.get(
  '/:id',
  (req, res, next) => {
    try {
      res.data = PostService.getPost({ id: req.params.id });
    } catch (err) {
      res.err = err;
      res.err.status = 404;
    } finally {
      next();
    }
  },
  responseMiddleware,
);

router.post(
  '/',
  (req, res, next) => {
    try {
      if (!res.err) {
        res.data = PostService.create(req.body);
      }
    } catch (err) {
      res.err = err;
      res.err.status = 400;
    } finally {
      next();
    }
  },
  responseMiddleware,
);

router.put(
  '/:id',
  (req, res, next) => {
    try {
      if (!res.err) {
        res.data = PostService.update(req.params.id, req.body);
      }
    } catch (err) {
      res.err = err;
      res.err.status = 400;
    } finally {
      next();
    }
  },
  responseMiddleware,
);

router.delete(
  '/:id',
  (req, res, next) => {
    try {
      res.data = PostService.delete(req.params.id);
    } catch (err) {
      res.err = err;
      res.err.status = 400;
    } finally {
      next();
    }
  },
  responseMiddleware,
);

module.exports = router;
