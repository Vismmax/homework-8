const { post } = require('../models/post');
const { BaseValidationService } = require('../services/baseValidationService');

class PostValidationService extends BaseValidationService {
  constructor() {
    super(post);
  }
}

exports.PostValidationService = new PostValidationService();
