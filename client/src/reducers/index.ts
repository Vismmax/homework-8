import { combineReducers } from 'redux';
import profile from '../containers/LoginPage/reducer';
import chat from '../containers/Chat/reducer';
import userList from '../containers/UserList/reducer';
import editableUser from '../containers/UserEditor/reducer';
import editablePost from '../containers/PostEditor/reducer';
import notification from '../components/Notification/reducer';

const rootReducer = combineReducers({
  profile,
  chat,
  userList,
  editableUser,
  editablePost,
  notification,
});

export default rootReducer;
