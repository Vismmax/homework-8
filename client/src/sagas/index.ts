import { all } from 'redux-saga/effects';
import authSagas from '../containers/LoginPage/sagas';
import chatSagas from '../containers/Chat/sagas';
import userListSagas from '../containers/UserList/sagas';
import userEditorSagas from '../containers/UserEditor/sagas';
import postEditorSagas from '../containers/PostEditor/sagas';

export default function* rootSaga() {
  yield all([authSagas(), chatSagas(), userListSagas(), userEditorSagas(), postEditorSagas()]);
}
