import React from 'react';
import PropTypes from 'prop-types';
import { Divider, Label } from 'semantic-ui-react';

type Props = {
  date: string;
};

function ChatDivider({ date }: Props) {
  return (
    <Divider horizontal>
      <Label>{date}</Label>
    </Divider>
  );
}

ChatDivider.propTypes = {
  date: PropTypes.string.isRequired,
};

export default ChatDivider;
