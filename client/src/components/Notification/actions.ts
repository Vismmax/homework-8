import { SHOW_NOTIFICATION } from './actionTypes';

type Props = {
  type: string;
  text: string;
  title?: string;
};

export const showNotificationAction = (notification: Props) => ({
  type: SHOW_NOTIFICATION,
  payload: notification,
});
