import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { NotificationContainer, NotificationManager } from 'react-notifications';
import { TypeRootState } from '../../models/types';

function Notification() {
  const notification = useSelector((state: TypeRootState) => state.notification);

  useEffect(() => {
    console.log('notification = useSelector', notification);
    const { type, text, title } = notification;
    switch (type) {
      case 'info':
        NotificationManager.info(text, title);
        break;
      case 'success':
        NotificationManager.success(text, title);
        break;
      case 'warning':
        NotificationManager.warning(text, title);
        break;
      case 'error':
        NotificationManager.error(text, title);
        break;
    }
  }, [notification]);

  return <NotificationContainer />;
}

export default Notification;
