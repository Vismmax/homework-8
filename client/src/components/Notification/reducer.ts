import { SHOW_NOTIFICATION } from './actionTypes';
import { AnyAction } from 'redux';

export type TypeNotificationState = {
  type: string;
  text: string;
  title?: string;
};

const initialState = {
  type: '',
  text: '',
  title: '',
};

export default function (state = initialState, action: AnyAction) {
  switch (action.type) {
    case SHOW_NOTIFICATION: {
      return {
        ...state,
        ...action.payload,
      };
    }

    default:
      return state;
  }
}
