import React from 'react';
import { Icon, Table, Button } from 'semantic-ui-react';
import { TypeUser } from '../../models/types';

type Props = {
  user: TypeUser;
  del: (id: string) => void;
  edit: (id: string) => void;
};

function UserListItem({ user: { id, name, surname, email, isAdmin }, del, edit }: Props) {
  return (
    <Table.Row>
      <Table.Cell>{name}</Table.Cell>
      <Table.Cell>{surname}</Table.Cell>
      <Table.Cell>{email}</Table.Cell>
      <Table.Cell>{isAdmin && <Icon color="green" name="checkmark" size="large" />}</Table.Cell>
      <Table.Cell>
        <Button.Group>
          <Button icon="trash" content="Delete" onClick={() => del(id)} />
          <Button icon="edit" content="Edit" onClick={() => edit(id)} />
        </Button.Group>
      </Table.Cell>
    </Table.Row>
  );
}

export default UserListItem;
