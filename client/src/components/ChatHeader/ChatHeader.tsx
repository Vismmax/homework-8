import React from 'react';
import PropTypes from 'prop-types';
import { Menu, Label } from 'semantic-ui-react';
import moment from 'moment';
import './ChatHeader.css';

type Props = {
  chatName: string;
  countUsers: number;
  countPosts: number;
  dateLastPost: string;
};

function ChatHeader({ chatName, countUsers, countPosts, dateLastPost }: Props) {
  return (
    <Menu className="chat-header">
      <Menu.Item header>{chatName}</Menu.Item>
      <Menu.Item>
        <Label color="blue">{countUsers} participants</Label>
      </Menu.Item>
      <Menu.Item>
        <Label color="blue">{countPosts} messages</Label>
      </Menu.Item>
      <Menu.Menu position="right">
        <Menu.Item>
          <Label>last message at {moment(dateLastPost).format('MMMM Do YYYY, hh:mm:ss')}</Label>
        </Menu.Item>
      </Menu.Menu>
    </Menu>
  );
}

ChatHeader.propTypes = {
  chatName: PropTypes.string.isRequired,
  countUsers: PropTypes.number.isRequired,
  countPosts: PropTypes.number.isRequired,
  dateLastPost: PropTypes.string.isRequired,
};

export default ChatHeader;
