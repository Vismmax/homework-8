import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { Icon, Menu } from 'semantic-ui-react';
import './PageHeader.css';
import logo from '../../img/logo.svg';
import { TypeRootState } from '../../models/types';
import { logoutUserAction } from '../../containers/LoginPage/actions';

function PageHeader() {
  const userName = useSelector((state: TypeRootState) => state.profile.user.name);
  const isAuthorized = useSelector((state: TypeRootState) => state.profile.isAuthorized);
  const isAdmin = useSelector((state: TypeRootState) => state.profile.isAdmin);
  const dispatch = useDispatch();

  const logout = () => {
    dispatch(logoutUserAction());
  };

  return (
    <Menu text className="page-header">
      <Menu.Item>
        <img src={logo} alt="Logo" />
      </Menu.Item>
      {isAdmin && (
        <Menu.Item>
          <Link to="/">Chat</Link>
        </Menu.Item>
      )}
      {isAdmin && (
        <Menu.Item>
          <Link to="/users">User List</Link>
        </Menu.Item>
      )}
      <Menu.Item header position="right">
        {userName}
      </Menu.Item>
      <Menu.Item>
        {isAuthorized && (
          <Icon className="link-logout" name="sign-out" title="Logout" link onClick={logout} />
        )}
      </Menu.Item>
    </Menu>
  );
}

export default PageHeader;
