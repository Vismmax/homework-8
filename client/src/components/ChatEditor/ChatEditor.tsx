import React, { FormEvent, useState } from 'react';
import PropTypes from 'prop-types';
import { Form, Button } from 'semantic-ui-react';
import './ChatEditor.css';

type Props = {
  onSubmit: (value: string) => void;
};

function ChatEditor({ onSubmit }: Props) {
  const [text, setText] = useState('');

  const onSubmitHandler = (ev: FormEvent) => {
    ev.preventDefault();
    onSubmit(text);
    setText('');
  };

  return (
    <Form className="chat-editor" onSubmit={onSubmitHandler}>
      <div className="textarea-wrap">
        <textarea rows={2} value={text} onChange={(e) => setText(e.target.value)} />
      </div>
      <div className="send-wrap">
        <Button
          size="huge"
          type="submit"
          icon={text ? 'paper plane' : 'edit'}
          content={text ? 'Send' : 'Open Editor'}
        />
      </div>
    </Form>
  );
}

ChatEditor.propTypes = {
  onSubmit: PropTypes.func.isRequired,
};

export default ChatEditor;
