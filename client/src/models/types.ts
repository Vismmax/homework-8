import { TypeProfileState } from '../containers/LoginPage/reducer';
import { TypePostsState } from '../containers/Chat/reducer';
import { TypeUsersState } from '../containers/UserList/reducer';
import { TypeUserState } from '../containers/UserEditor/reducer';
import { TypePostState } from '../containers/PostEditor/reducer';
import { TypeNotificationState } from '../components/Notification/reducer';

export type TypePost = {
  id: string;
  text: string;
  user: string;
  userId: string;
  avatar: string;
  createdAt: string;
  editedAt: string;
  isLikes?: boolean;
};

export type TypeUser = {
  id: string;
  name: string;
  surname: string;
  email: string;
  password: string;
  isAdmin: boolean;
};

export type TypeRootState = {
  profile: TypeProfileState;
  chat: TypePostsState;
  userList: TypeUsersState;
  editableUser: TypeUserState;
  editablePost: TypePostState;
  notification: TypeNotificationState;
};
