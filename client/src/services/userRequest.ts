import { deleteReq, get, post, put } from './requestHelper';
import { TypeUser } from '../models/types';
const entity = 'users';

export const getAllUsersRequest = async () => {
  return await get(entity);
};

export const getUserRequest = async (id: string) => {
  return await get(entity, id);
};

export const createUserRequest = async (body: TypeUser) => {
  return await post(entity, body);
};

export const updateUserRequest = async (id: string, body: TypeUser) => {
  return await put(entity, id, body);
};

export const deleteUserRequest = async (id: string) => {
  return await deleteReq(entity, id);
};
