const apiUrl = '/api';

export const get = async (entityName: string, id = '') => {
  console.log('get', `${entityName}/${id}`);
  return await makeRequest(`${entityName}/${id}`, 'GET');
};

export const post = async (entityName: string, body: {}) => {
  return await makeRequest(entityName, 'POST', body);
};

export const put = async (entityName: string, id: string, body: {}) => {
  return await makeRequest(`${entityName}/${id}`, 'PUT', body);
};

export const deleteReq = async (entityName: string, id: string) => {
  return await makeRequest(`${entityName}/${id}`, 'DELETE');
};

const makeRequest = async (path: string, method: string, body: {} | null = null) => {
  try {
    const url = `${apiUrl}/${path}`;
    console.log('url', url);
    const res = await fetch(url, {
      method,
      body: body ? JSON.stringify(body) : undefined,
      headers: { 'Content-Type': 'application/json' },
    });

    const dataObj = await res.json();

    if (res.ok) {
      return dataObj;
    }

    alert(`${dataObj.message}`);
    return dataObj;
  } catch (err) {
    console.error(err);
  }
};
