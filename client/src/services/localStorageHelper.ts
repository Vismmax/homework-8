export const setLocalStorageItem = (key: string, val: string | {} | null) => {
  const value = typeof val === 'object' ? JSON.stringify(val) : val;
  localStorage.setItem(key, value as string);
};

export const getObjectFromLocalStorage = (key: string) => {
  const data = localStorage.getItem(key);
  if (data) {
    return JSON.parse(data);
  }
  return null;
};
