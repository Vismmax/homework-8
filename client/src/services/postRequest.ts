import { deleteReq, get, post, put } from './requestHelper';
import { TypePost } from '../models/types';
const entity = 'posts';

export const getAllPostsRequest = async () => {
  return await get(entity);
};

export const getPostRequest = async (id: string) => {
  return await get(entity, id);
};

export const createPostRequest = async (body: TypePost) => {
  return await post(entity, body);
};

export const updatePostRequest = async (id: string, body: TypePost) => {
  return await put(entity, id, body);
};

export const deletePostRequest = async (id: string) => {
  return await deleteReq(entity, id);
};
