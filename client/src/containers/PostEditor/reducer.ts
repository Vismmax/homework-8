import { FETCH_POST_REQUEST, FETCH_POST_SUCCESS, FETCH_POST_FAILURE } from './actionTypes';
import { TypePost } from '../../models/types';
import { AnyAction } from 'redux';

export type TypePostState = {
  post: TypePost;
  isLoading: boolean;
};

const initialState = {
  post: {},
  isLoading: false,
} as TypePostState;

export default function (state = initialState, action: AnyAction) {
  switch (action.type) {
    case FETCH_POST_REQUEST: {
      return {
        ...state,
        isLoading: true,
      };
    }

    case FETCH_POST_SUCCESS: {
      return {
        post: action.payload,
        isLoading: false,
      };
    }

    case FETCH_POST_FAILURE: {
      return {
        ...state,
        isLoading: false,
      };
    }

    default:
      return state;
  }
}
