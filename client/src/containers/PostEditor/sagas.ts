import { call, put, takeEvery, all } from 'redux-saga/effects';
import { FETCH_POST_REQUEST, FETCH_POST_SUCCESS, FETCH_POST_FAILURE } from './actionTypes';
import { getPostRequest } from '../../services/postRequest';
import { AnyAction } from 'redux';
import { SHOW_NOTIFICATION } from '../../components/Notification/actionTypes';

export function* fetchPost(action: AnyAction) {
  try {
    const posts = yield call(getPostRequest, action.payload);
    yield put({ type: FETCH_POST_SUCCESS, payload: posts });
  } catch (error) {
    yield put({ type: FETCH_POST_FAILURE, payload: error.message });
    yield put({
      type: SHOW_NOTIFICATION,
      payload: { type: 'error', text: error.message, title: 'Error' },
    });
  }
}

function* watchFetchPost() {
  yield takeEvery(FETCH_POST_REQUEST, fetchPost);
}

export default function* chatSagas() {
  yield all([watchFetchPost()]);
}
