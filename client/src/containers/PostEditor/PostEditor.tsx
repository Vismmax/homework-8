import React, { FormEvent, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useParams } from 'react-router';
import { Segment, Button, Form } from 'semantic-ui-react';
import './PostEditor.css';
import { TypeRootState } from '../../models/types';
import { addPostAction, updatePostAction } from '../Chat/actions';
import { fetchPostsAction } from './actions';

function PostEditor() {
  const { id } = useParams();
  const post = useSelector((state: TypeRootState) => state.editablePost.post);
  const isLoading = useSelector((state: TypeRootState) => state.editablePost.isLoading);
  const currentUser = useSelector((state: TypeRootState) => state.profile.user);
  const dispatch = useDispatch();
  const history = useHistory();

  const [text, setText] = useState('');

  useEffect(() => {
    if (id) dispatch(fetchPostsAction(id));
  }, [id]);

  useEffect(() => {
    setText(post.text);
  }, [post]);

  const onSubmitHandler = (ev: FormEvent) => {
    ev.preventDefault();
    if (!text) return;
    if (id) {
      dispatch(
        updatePostAction({
          ...post,
          text,
        }),
      );
    } else {
      dispatch(addPostAction(text, currentUser));
    }
    history.push('/');
  };

  const onCancelHandler = () => {
    history.push('/');
  };

  return (
    <Segment className="post-editor" size="large" loading={isLoading}>
      <Form size="big" onSubmit={onSubmitHandler}>
        <h1 className="ui dividing header">{id ? 'Edit Post' : 'New Post'}</h1>
        <Form.Field>
          <textarea
            rows={3}
            placeholder="Post"
            value={text}
            onChange={(e) => setText(e.target.value)}
          />
        </Form.Field>
        <Button basic icon="cancel" content="Cancel" onClick={onCancelHandler} />
        <Button type="submit" icon="save" content="Save" floated="right" disabled={!text} />
      </Form>
    </Segment>
  );
}

export default PostEditor;
