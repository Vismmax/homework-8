import { FETCH_POST_REQUEST } from './actionTypes';

export const fetchPostsAction = (id: string) => ({
  type: FETCH_POST_REQUEST,
  payload: id,
});
