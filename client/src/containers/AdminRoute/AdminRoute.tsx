import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { TypeRootState } from '../../models/types';

type Props = {
  exact?: boolean;
  path: string;
  component: any;
};

const AdminRoute = ({ component: Component, ...rest }: Props) => {
  const isAdmin = useSelector((state: TypeRootState) => state.profile.isAdmin);
  return (
    <Route
      {...rest}
      render={(props) => (isAdmin ? <Component {...props} /> : <Redirect to="/login" />)}
    />
  );
};

export default AdminRoute;
