import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { TypeRootState } from '../../models/types';

type Props = {
  exact?: boolean;
  path: string;
  component: any;
};

const PrivateRoute = ({ component: Component, ...rest }: Props) => {
  const isAuthorized = useSelector((state: TypeRootState) => state.profile.isAuthorized);
  return (
    <Route
      {...rest}
      render={(props) => (isAuthorized ? <Component {...props} /> : <Redirect to="/login" />)}
    />
  );
};

export default PrivateRoute;
