import { call, put, takeEvery, all } from 'redux-saga/effects';
import { FETCH_USER_REQUEST, FETCH_USER_SUCCESS, FETCH_USER_FAILURE } from './actionTypes';
import { getUserRequest } from '../../services/userRequest';
import { AnyAction } from 'redux';
import { SHOW_NOTIFICATION } from '../../components/Notification/actionTypes';

export function* fetchUser(action: AnyAction) {
  try {
    const users = yield call(getUserRequest, action.payload);
    yield put({ type: FETCH_USER_SUCCESS, payload: users });
  } catch (error) {
    yield put({ type: FETCH_USER_FAILURE, payload: error.message });
    yield put({
      type: SHOW_NOTIFICATION,
      payload: { type: 'error', text: error.message, title: 'Error' },
    });
  }
}

function* watchFetchUser() {
  yield takeEvery(FETCH_USER_REQUEST, fetchUser);
}

export default function* userEditorSagas() {
  yield all([watchFetchUser()]);
}
