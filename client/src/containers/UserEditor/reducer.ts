import { FETCH_USER_REQUEST, FETCH_USER_SUCCESS, FETCH_USER_FAILURE } from './actionTypes';
import { AnyAction } from 'redux';
import { TypeUser } from '../../models/types';

export type TypeUserState = {
  user: TypeUser;
  isLoading: boolean;
};

const initialState = {
  user: {},
  isLoading: false,
} as TypeUserState;

export default function (state = initialState, action: AnyAction) {
  switch (action.type) {
    case FETCH_USER_REQUEST: {
      return {
        ...state,
        isLoading: true,
      };
    }

    case FETCH_USER_SUCCESS: {
      return {
        user: action.payload,
        isLoading: false,
      };
    }

    case FETCH_USER_FAILURE: {
      return {
        ...state,
        isLoading: false,
      };
    }

    default:
      return state;
  }
}
