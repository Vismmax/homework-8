import { FETCH_USER_REQUEST } from './actionTypes';

export const fetchUserAction = (id: string) => ({
  type: FETCH_USER_REQUEST,
  payload: id,
});
