import React, { FormEvent, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useParams } from 'react-router';
import { Segment, Button, Form, Input, Icon, Checkbox } from 'semantic-ui-react';
import './UserEditor.css';
import { TypeRootState } from '../../models/types';
import { fetchUserAction } from './actions';
import { addUserAction, updateUserAction } from '../UserList/actions';

function UserEditor() {
  const { id } = useParams();
  const user = useSelector((state: TypeRootState) => state.editableUser.user);
  const isLoading = useSelector((state: TypeRootState) => state.editableUser.isLoading);
  const dispatch = useDispatch();
  const history = useHistory();

  const [name, setName] = useState('');
  const [surname, setSurname] = useState('');
  const [email, setEmail] = useState('');
  const [isAdmin, setIsAdmin] = useState(false);
  const [password, setPassword] = useState('');
  const [showPassword, setShowPassword] = useState(false);

  useEffect(() => {
    if (id) dispatch(fetchUserAction(id));
  }, [id]);

  useEffect(() => {
    if (user.id) {
      setName(user.name);
      setSurname(user.surname);
      setEmail(user.email);
      setPassword(user.password);
      setIsAdmin(user.isAdmin);
    }
  }, [user]);

  const validateEmptyFields = () => {
    return !(!name || !email || !password);
  };

  const updateUser = () => {
    dispatch(
      updateUserAction({
        ...user,
        name,
        surname,
        email,
        isAdmin,
        password,
      }),
    );
  };

  const addUser = () => {
    dispatch(
      addUserAction({
        id: '',
        name,
        surname,
        email,
        isAdmin,
        password,
      }),
    );
  };

  const onSubmitHandler = (ev: FormEvent) => {
    ev.preventDefault();
    if (id) updateUser();
    else addUser();
    history.push('/users');
  };

  const onCancelHandler = () => {
    history.push('/users');
  };

  return (
    <Segment className="user-editor" size="large" loading={isLoading}>
      <Form size="big" onSubmit={onSubmitHandler}>
        <h1 className="ui dividing header">{id ? 'Edit User' : 'New User'}</h1>
        <Form.Field>
          <label>User Name</label>
          <input
            type="text"
            name="name"
            placeholder="User Name"
            value={name}
            onChange={(e) => setName(e.target.value)}
          />
        </Form.Field>
        <Form.Field>
          <label>User Surname</label>
          <input
            type="text"
            name="surname"
            placeholder="User Surname"
            value={surname}
            onChange={(e) => setSurname(e.target.value)}
          />
        </Form.Field>
        <Form.Field>
          <label>User Email</label>
          <input
            type="email"
            name="email"
            placeholder="User Email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
        </Form.Field>
        <Form.Field>
          <label>User Password</label>
          <Input
            type={showPassword ? 'text' : 'password'}
            name="password"
            placeholder="User Password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            icon={
              <Icon
                link
                name={showPassword ? 'eye slash' : 'eye'}
                onClick={() => {
                  setShowPassword(!showPassword);
                }}
              />
            }
          />
        </Form.Field>
        <Form.Field>
          <Checkbox
            label="User Is Admin"
            name="isAdmin"
            checked={isAdmin}
            onChange={() => setIsAdmin(!isAdmin)}
          />
        </Form.Field>
        <Button basic icon="cancel" content="Cancel" onClick={onCancelHandler} />
        <Button
          type="submit"
          icon="save"
          content="Save"
          floated="right"
          disabled={!validateEmptyFields()}
        />
      </Form>
    </Segment>
  );
}

export default UserEditor;
