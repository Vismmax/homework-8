import {
  FETCH_USERS_REQUEST,
  ADD_USER_REQUEST,
  UPDATE_USER_REQUEST,
  DELETE_USER_REQUEST,
} from './actionTypes';
import { TypeUser } from '../../models/types';

export const fetchUsersAction = () => ({
  type: FETCH_USERS_REQUEST,
});

export const addUserAction = (user: TypeUser) => ({
  type: ADD_USER_REQUEST,
  payload: user,
});

export const updateUserAction = (user: TypeUser) => ({
  type: UPDATE_USER_REQUEST,
  payload: user,
});

export const deleteUserAction = (id: string) => ({
  type: DELETE_USER_REQUEST,
  payload: id,
});
