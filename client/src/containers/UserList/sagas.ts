import { call, put, takeEvery, all } from 'redux-saga/effects';
import {
  FETCH_USERS_REQUEST,
  FETCH_USERS_SUCCESS,
  FETCH_USERS_FAILURE,
  ADD_USER_REQUEST,
  UPDATE_USER_REQUEST,
  DELETE_USER_REQUEST,
} from './actionTypes';
import {
  getAllUsersRequest,
  createUserRequest,
  updateUserRequest,
  deleteUserRequest,
} from '../../services/userRequest';
import { AnyAction } from 'redux';
import { SHOW_NOTIFICATION } from '../../components/Notification/actionTypes';

export function* fetchUsers(action: AnyAction) {
  try {
    const users = yield call(getAllUsersRequest);
    yield put({ type: FETCH_USERS_SUCCESS, payload: users });
  } catch (error) {
    yield put({ type: FETCH_USERS_FAILURE, payload: error.message });
    yield put({
      type: SHOW_NOTIFICATION,
      payload: { type: 'error', text: error.message, title: 'Error' },
    });
  }
}

function* watchFetchUsers() {
  yield takeEvery(FETCH_USERS_REQUEST, fetchUsers);
}

export function* addUser(action: AnyAction) {
  try {
    yield call(createUserRequest, action.payload);
    yield put({ type: FETCH_USERS_REQUEST });
    yield put({
      type: SHOW_NOTIFICATION,
      payload: { type: 'success', text: 'User added successfully' },
    });
  } catch (error) {
    yield put({ type: FETCH_USERS_FAILURE, payload: error.message });
    yield put({
      type: SHOW_NOTIFICATION,
      payload: { type: 'error', text: error.message, title: 'Error' },
    });
  }
}

function* watchAddUser() {
  yield takeEvery(ADD_USER_REQUEST, addUser);
}

export function* updateUser(action: AnyAction) {
  try {
    yield call(updateUserRequest, action.payload.id, action.payload);
    yield put({ type: FETCH_USERS_REQUEST });
    yield put({
      type: SHOW_NOTIFICATION,
      payload: { type: 'success', text: 'User updated successfully' },
    });
  } catch (error) {
    yield put({ type: FETCH_USERS_FAILURE, payload: error.message });
    yield put({
      type: SHOW_NOTIFICATION,
      payload: { type: 'error', text: error.message, title: 'Error' },
    });
  }
}

function* watchUpdateUser() {
  yield takeEvery(UPDATE_USER_REQUEST, updateUser);
}

export function* deleteUser(action: AnyAction) {
  try {
    yield call(deleteUserRequest, action.payload);
    yield put({ type: FETCH_USERS_REQUEST });
    yield put({
      type: SHOW_NOTIFICATION,
      payload: { type: 'success', text: 'User deleted successfully' },
    });
  } catch (error) {
    yield put({ type: FETCH_USERS_FAILURE, payload: error.message });
    yield put({
      type: SHOW_NOTIFICATION,
      payload: { type: 'error', text: error.message, title: 'Error' },
    });
  }
}

function* watchDeleteUser() {
  yield takeEvery(DELETE_USER_REQUEST, deleteUser);
}

export default function* userListSagas() {
  yield all([watchFetchUsers(), watchAddUser(), watchUpdateUser(), watchDeleteUser()]);
}
