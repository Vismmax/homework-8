import { FETCH_USERS_REQUEST, FETCH_USERS_SUCCESS, FETCH_USERS_FAILURE } from './actionTypes';
import { TypeUser } from '../../models/types';
import { AnyAction } from 'redux';

export type TypeUsersState = {
  users: TypeUser[];
  isLoading: boolean;
};

const initialState = {
  users: [],
  isLoading: false,
} as TypeUsersState;

export default function (state = initialState, action: AnyAction) {
  switch (action.type) {
    case FETCH_USERS_REQUEST: {
      return {
        ...state,
        isLoading: true,
      };
    }

    case FETCH_USERS_SUCCESS: {
      return {
        users: action.payload,
        isLoading: false,
      };
    }

    case FETCH_USERS_FAILURE: {
      return {
        ...state,
        isLoading: false,
      };
    }

    default:
      return state;
  }
}
