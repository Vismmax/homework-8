import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import { Table, Segment, Button } from 'semantic-ui-react';
import './UserList.css';
import { TypeRootState } from '../../models/types';
import UserListItem from '../../components/UserListItem/UserListItem';
import { deleteUserAction, fetchUsersAction } from './actions';

function UserList() {
  const users = useSelector((state: TypeRootState) => state.userList.users);
  const isLoading = useSelector((state: TypeRootState) => state.userList.isLoading);
  const dispatch = useDispatch();
  const history = useHistory();

  useEffect(() => {
    dispatch(fetchUsersAction());
  }, []);

  const addUser = () => {
    history.push('/user');
  };

  const editUser = (id: string) => {
    history.push(`/user/${id}`);
  };

  const deleteUser = (id: string) => {
    dispatch(deleteUserAction(id));
  };

  const userListItems = users.map((user) => (
    <UserListItem key={user.id} user={user} del={deleteUser} edit={editUser} />
  ));

  return (
    <Segment size="large" loading={isLoading}>
      <div className="button-add-wrap">
        <Button icon="add" content="Add User" onClick={() => addUser()} />
      </div>

      <Table singleLine>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>Name</Table.HeaderCell>
            <Table.HeaderCell>Surname</Table.HeaderCell>
            <Table.HeaderCell>E-mail address</Table.HeaderCell>
            <Table.HeaderCell>Is Admin</Table.HeaderCell>
            <Table.HeaderCell>Actions</Table.HeaderCell>
          </Table.Row>
        </Table.Header>

        <Table.Body>{userListItems}</Table.Body>
      </Table>
    </Segment>
  );
}

export default UserList;
