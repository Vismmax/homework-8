import {
  FETCH_POSTS_REQUEST,
  ADD_POST_REQUEST,
  UPDATE_POST_REQUEST,
  DELETE_POST_REQUEST,
  LIKE_POST_REQUEST,
} from './actionTypes';
import { TypePost, TypeUser } from '../../models/types';

export const fetchPostsAction = () => ({
  type: FETCH_POSTS_REQUEST,
});

export const addPostAction = (text: string, user: TypeUser) => ({
  type: ADD_POST_REQUEST,
  payload: {
    text: text,
    user: user.name,
    userId: user.id,
    avatar: '',
  },
});

export const updatePostAction = (post: TypePost) => ({
  type: UPDATE_POST_REQUEST,
  payload: post,
});

export const deletePostAction = (post: TypePost) => ({
  type: DELETE_POST_REQUEST,
  payload: post.id,
});

export const likePostAction = (post: TypePost) => ({
  type: LIKE_POST_REQUEST,
  payload: post,
});
