import {
  FETCH_POSTS_REQUEST,
  FETCH_POSTS_SUCCESS,
  FETCH_POSTS_FAILURE,
  LIKE_POST_SUCCESS,
} from './actionTypes';
import { TypePost } from '../../models/types';
import { AnyAction } from 'redux';

export type TypePostsState = {
  posts: TypePost[];
  isLoading: boolean;
};

const initialState = {
  posts: [],
  isLoading: false,
} as TypePostsState;

export default function (state = initialState, action: AnyAction) {
  switch (action.type) {
    case FETCH_POSTS_REQUEST: {
      return {
        ...state,
        isLoading: true,
      };
    }

    case FETCH_POSTS_SUCCESS: {
      return {
        posts: action.payload,
        isLoading: false,
      };
    }

    case FETCH_POSTS_FAILURE: {
      return {
        ...state,
        isLoading: false,
      };
    }

    case LIKE_POST_SUCCESS: {
      const posts = state.posts.map((post) =>
        post.id === action.payload.id ? action.payload : post,
      );
      return {
        ...state,
        posts,
        isLoading: false,
      };
    }

    default:
      return state;
  }
}
