import { call, put, takeEvery, all } from 'redux-saga/effects';
import {
  FETCH_POSTS_REQUEST,
  FETCH_POSTS_SUCCESS,
  FETCH_POSTS_FAILURE,
  ADD_POST_REQUEST,
  UPDATE_POST_REQUEST,
  DELETE_POST_REQUEST,
  LIKE_POST_REQUEST,
  LIKE_POST_SUCCESS,
  LIKE_POST_FAILURE,
} from './actionTypes';
import {
  getAllPostsRequest,
  createPostRequest,
  updatePostRequest,
  deletePostRequest,
} from '../../services/postRequest';
import { AnyAction } from 'redux';
import { SHOW_NOTIFICATION } from '../../components/Notification/actionTypes';

export function* fetchPosts(action: AnyAction) {
  try {
    const posts = yield call(getAllPostsRequest);
    yield put({ type: FETCH_POSTS_SUCCESS, payload: posts });
  } catch (error) {
    yield put({ type: FETCH_POSTS_FAILURE, payload: error.message });
    yield put({
      type: SHOW_NOTIFICATION,
      payload: { type: 'error', text: error.message, title: 'Error' },
    });
  }
}

function* watchFetchPosts() {
  yield takeEvery(FETCH_POSTS_REQUEST, fetchPosts);
}

export function* addPost(action: AnyAction) {
  try {
    yield call(createPostRequest, action.payload);
    yield put({ type: FETCH_POSTS_REQUEST });
    yield put({
      type: SHOW_NOTIFICATION,
      payload: { type: 'success', text: 'Post added successfully' },
    });
  } catch (error) {
    yield put({ type: FETCH_POSTS_FAILURE, payload: error.message });
    yield put({
      type: SHOW_NOTIFICATION,
      payload: { type: 'error', text: error.message, title: 'Error' },
    });
  }
}

function* watchAddPost() {
  yield takeEvery(ADD_POST_REQUEST, addPost);
}

export function* updatePost(action: AnyAction) {
  try {
    yield call(updatePostRequest, action.payload.id, action.payload);
    yield put({ type: FETCH_POSTS_REQUEST });
    yield put({
      type: SHOW_NOTIFICATION,
      payload: { type: 'success', text: 'Post updated successfully' },
    });
  } catch (error) {
    yield put({ type: FETCH_POSTS_FAILURE, payload: error.message });
    yield put({
      type: SHOW_NOTIFICATION,
      payload: { type: 'error', text: error.message, title: 'Error' },
    });
  }
}

function* watchUpdatePost() {
  yield takeEvery(UPDATE_POST_REQUEST, updatePost);
}

export function* deletePost(action: AnyAction) {
  try {
    yield call(deletePostRequest, action.payload);
    yield put({ type: FETCH_POSTS_REQUEST });
    yield put({
      type: SHOW_NOTIFICATION,
      payload: { type: 'success', text: 'Post deleted successfully' },
    });
  } catch (error) {
    yield put({ type: FETCH_POSTS_FAILURE, payload: error.message });
    yield put({
      type: SHOW_NOTIFICATION,
      payload: { type: 'error', text: error.message, title: 'Error' },
    });
  }
}

function* watchDeletePost() {
  yield takeEvery(DELETE_POST_REQUEST, deletePost);
}

export function* likePost(action: AnyAction) {
  try {
    yield call(updatePostRequest, action.payload.id, action.payload);
    yield put({ type: LIKE_POST_SUCCESS, payload: action.payload });
    yield put({
      type: SHOW_NOTIFICATION,
      payload: { type: 'success', text: 'Post liked successfully' },
    });
  } catch (error) {
    yield put({ type: LIKE_POST_FAILURE, payload: error.message });
    yield put({
      type: SHOW_NOTIFICATION,
      payload: { type: 'error', text: error.message, title: 'Error' },
    });
  }
}

function* watchLikePost() {
  yield takeEvery(LIKE_POST_REQUEST, likePost);
}

export default function* chatSagas() {
  yield all([
    watchFetchPosts(),
    watchAddPost(),
    watchUpdatePost(),
    watchDeletePost(),
    watchLikePost(),
  ]);
}
