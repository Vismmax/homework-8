import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import './Chat.css';
import { TypePost, TypeRootState } from '../../models/types';
import { addPostAction, deletePostAction, fetchPostsAction, likePostAction } from './actions';
import Spinner from '../../components/Spinner/Spinner';
import ChatHeader from '../../components/ChatHeader/ChatHeader';
import ChatPosts from '../../components/ChatPosts/ChatPosts';
import ChatEditor from '../../components/ChatEditor/ChatEditor';

function Chat() {
  const posts = useSelector((state: TypeRootState) => state.chat.posts);
  const isLoading = useSelector((state: TypeRootState) => state.chat.isLoading);
  const currentUser = useSelector((state: TypeRootState) => state.profile.user);
  const dispatch = useDispatch();
  const history = useHistory();

  useEffect(() => {
    dispatch(fetchPostsAction());
  }, []);

  useEffect(() => {
    document.addEventListener('keyup', keyUpHandler);
    return () => {
      document.removeEventListener('keyup', keyUpHandler);
    };
  }, [currentUser, posts]);

  const keyUpHandler = (ev: KeyboardEvent) => {
    if (ev.key === 'ArrowUp') {
      const postsCurrentUser = posts.filter((post) => post.userId === currentUser.id);
      const post = postsCurrentUser[postsCurrentUser.length - 1];
      if (post) editPost(post);
    }
  };

  const countUsers = () => {
    const users = new Set(posts.map((post) => post.user));
    return users.size;
  };

  const addPost = (text: string) => {
    console.log('text, currentUser', text, currentUser);
    dispatch(addPostAction(text, currentUser));
  };

  const deletePost = (post: TypePost): void => {
    dispatch(deletePostAction(post));
  };

  const editPost = (post: TypePost): void => {
    history.push(`/post/${post.id}`);
  };

  const likePost = (post: TypePost): void => {
    dispatch(
      likePostAction({
        ...post,
        isLikes: !post.isLikes,
      }),
    );
  };

  const onSubmitTextHandler = (text: string) => {
    if (text) {
      addPost(text);
    } else {
      history.push('/post');
    }
  };

  return (
    <div className="chat ui container">
      <ChatHeader
        chatName={'Chat'}
        countUsers={countUsers()}
        countPosts={posts.length}
        dateLastPost={posts.length ? posts[posts.length - 1].createdAt : ''}
      />
      <ChatPosts
        posts={posts}
        currentUser={currentUser}
        editPost={editPost}
        deletePost={deletePost}
        likePost={likePost}
      />
      <ChatEditor onSubmit={onSubmitTextHandler} />
      {isLoading && <Spinner />}
    </div>
  );
}

export default Chat;
