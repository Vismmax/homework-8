import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { TypeRootState } from '../../models/types';

type Props = {
  path: string;
  exact?: boolean;
  component: any;
};

const PublicRoute = ({ component: Component, ...rest }: Props) => {
  const isAuthorized = useSelector((state: TypeRootState) => state.profile.isAuthorized);
  const isAdmin = useSelector((state: TypeRootState) => state.profile.isAdmin);
  return (
    <Route
      {...rest}
      render={(props) =>
        isAuthorized ? (
          isAdmin ? (
            <Redirect to="/users" />
          ) : (
            <Redirect to="/" />
          )
        ) : (
          <Component {...props} />
        )
      }
    />
  );
};

export default PublicRoute;
