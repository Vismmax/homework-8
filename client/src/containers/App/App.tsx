import React from 'react';
import { Switch } from 'react-router-dom';
import { Container } from 'semantic-ui-react';
import './App.css';
import PageHeader from '../../components/PageHeader/PageHeader';
import PageFooter from '../../components/PageFooter/PageFooter';
import LoginPage from '../LoginPage/LoginPage';
import Chat from '../Chat/Chat';
import UserList from '../UserList/UserList';
import UserEditor from '../UserEditor/UserEditor';
import PostEditor from '../PostEditor/PostEditor';
import PublicRoute from '../PublicRoute/PublicRoute';
import PrivateRoute from '../PrivateRoute/PrivateRoute';
import AdminRoute from '../AdminRoute/AdminRoute';
import Notification from '../../components/Notification/Notification';

function App() {
  return (
    <div className="app">
      <PageHeader />
      <Container className="app-content">
        <Switch>
          <PublicRoute exact path="/login" component={LoginPage} />
          <PrivateRoute exact path="/" component={Chat} />
          <PrivateRoute exact path="/post" component={PostEditor} />
          <PrivateRoute path="/post/:id" component={PostEditor} />
          <AdminRoute exact path="/users" component={UserList} />
          <AdminRoute exact path="/user" component={UserEditor} />
          <AdminRoute path="/user/:id" component={UserEditor} />
        </Switch>
      </Container>
      <PageFooter />
      <Notification />
    </div>
  );
}

export default App;
