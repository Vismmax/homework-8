import { call, put, takeEvery, all } from 'redux-saga/effects';
import { LOGIN_USER, LOGOUT_USER, SET_PROFILE, GET_USER } from './actionTypes';
import { login } from '../../services/authRequest';
import { AnyAction } from 'redux';
import { getObjectFromLocalStorage } from '../../services/localStorageHelper';
import { SHOW_NOTIFICATION } from '../../components/Notification/actionTypes';
import { setLoginSession, unsetLoginSession } from '../../services/authService';

export function* loginUser(action: AnyAction) {
  try {
    const user = yield call(login, action.payload);
    yield setLoginSession(user);
    yield put({ type: SET_PROFILE, payload: user });
  } catch (error) {
    yield put({ type: SET_PROFILE, payload: {} });
    yield put({
      type: SHOW_NOTIFICATION,
      payload: { type: 'error', text: error.message, title: 'Error' },
    });
  }
}

function* watchLoginUser() {
  yield takeEvery(LOGIN_USER, loginUser);
}

export function* logoutUser(action: AnyAction) {
  try {
    yield unsetLoginSession();
    yield put({ type: SET_PROFILE, payload: {} });
  } catch (error) {
    yield put({ type: SET_PROFILE, payload: {} });
    yield put({
      type: SHOW_NOTIFICATION,
      payload: { type: 'error', text: error.message, title: 'Error' },
    });
  }
}

function* watchLogoutUser() {
  yield takeEvery(LOGOUT_USER, logoutUser);
}

export function* getUser(action: AnyAction) {
  try {
    const user = getObjectFromLocalStorage('user');
    yield put({ type: SET_PROFILE, payload: user });
  } catch (error) {
    yield put({ type: SET_PROFILE, payload: {} });
  }
}

function* watchGetUser() {
  yield takeEvery(GET_USER, getUser);
}

export default function* authSagas() {
  yield all([watchLoginUser(), watchLogoutUser(), watchGetUser()]);
}
