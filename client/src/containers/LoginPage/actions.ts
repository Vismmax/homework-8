import { LOGIN_USER, LOGOUT_USER, GET_USER } from './actionTypes';

type TypeUser = {
  name: string;
  password: string;
};

export const loginUserAction = (user: TypeUser) => ({
  type: LOGIN_USER,
  payload: user,
});

export const logoutUserAction = () => ({
  type: LOGOUT_USER,
});

export const getUserAction = () => ({
  type: GET_USER,
});
