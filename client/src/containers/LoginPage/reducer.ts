import { LOGIN_USER, LOGOUT_USER, SET_PROFILE } from './actionTypes';
import { TypeUser } from '../../models/types';
import { AnyAction } from 'redux';

export type TypeProfileState = {
  user: TypeUser;
  isAuthorized: boolean;
  isLoading: boolean;
  isAdmin: boolean;
};

const initialState = {
  user: {},
  isAuthorized: false,
  isLoading: false,
  isAdmin: false,
} as TypeProfileState;

export default function (state = initialState, action: AnyAction) {
  switch (action.type) {
    case LOGIN_USER: {
      return {
        ...state,
        isLoading: true,
      };
    }

    case LOGOUT_USER: {
      return {
        ...state,
        isLoading: true,
      };
    }

    case SET_PROFILE: {
      const user = action.payload;
      return {
        user,
        isAuthorized: !!user.id,
        isLoading: false,
        isAdmin: user.isAdmin,
      };
    }

    default:
      return state;
  }
}
