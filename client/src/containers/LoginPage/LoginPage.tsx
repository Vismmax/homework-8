import React, { FormEvent, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Button, Input, Segment, Form, Icon } from 'semantic-ui-react';
import { getUserAction, loginUserAction } from './actions';
import { TypeRootState } from '../../models/types';

function LoginPage() {
  const [name, setName] = useState('');
  const [password, setPassword] = useState('');
  const [showPassword, setShowPassword] = useState(false);
  const dispatch = useDispatch();
  const isLoading = useSelector((state: TypeRootState) => state.profile.isLoading);

  useEffect(() => {
    dispatch(getUserAction());
  }, []);

  const onSubmitHandler = (ev: FormEvent) => {
    ev.preventDefault();
    dispatch(loginUserAction({ name, password }));
    setName('');
    setPassword('');
    setShowPassword(false);
  };

  return (
    <Segment size="massive" loading={isLoading}>
      <Form size="big" onSubmit={onSubmitHandler}>
        <h1 className="ui dividing header">Login</h1>
        <Form.Field>
          <label>User Name</label>
          <input
            type="text"
            name="name"
            placeholder="User Name"
            value={name}
            onChange={(e) => setName(e.target.value)}
          />
        </Form.Field>
        <Form.Field>
          <label>Password</label>
          <Input
            type={showPassword ? 'text' : 'password'}
            name="password"
            placeholder="Password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            icon={
              <Icon
                link
                name={showPassword ? 'eye slash' : 'eye'}
                onClick={() => {
                  setShowPassword(!showPassword);
                }}
              />
            }
          />
        </Form.Field>
        <Button type="submit">Login</Button>
      </Form>
    </Segment>
  );
}

export default LoginPage;
